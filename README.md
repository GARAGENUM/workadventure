# WORKADVENTURE 

![](https://sdtimes.com/wp-content/uploads/2020/12/workadventure.jpg)

## Description

Ce projet a pour but de déployer la stack workadventure (https://github.com/thecodingmachine/workadventure) en auto-hébergement.

## Pré-requis

Pour déployer cette stack, vous aurez besoin:
- Une machine pour héberger le serveur (proc 3Ghz, 4Go ram minimum)
- Un nom de domaine pour pointer vers votre serveur (*.adventure.nom-de-domain.tld)
- L'accès à votre box internet pour la redirection des ports

## Configuration / Installation

### Configuration

- Renseigner le nom de domaine aux quatres endroits:
```bash
nano .env
```

### Deployement

```bash
docker-compose up -d
```

### Map building

tuto: https://workadventu.re/map-building/

## OIDC

> Créer une configuration sur Keycloak pour le client workadventure
> Renseigner l'adresse du Keycloak, l'id et le secret client dans le .env

## JITSI 

> https://git.legaragenumerique.fr/GARAGENUM/jitsi

### Contributing

- Me
- The others

### License

... OPEN SOURCE GPL3
