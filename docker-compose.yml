version: "3.8"

services:
  
  reverse-proxy:
    image: traefik:v2.5.6
    container_name: adventure_traefik
    command:
      # for web ui traefik
      - "--api.insecure=true"
      - "--providers.docker=true"
      - "--providers.docker.swarmmode=false"
      - "--log.level=DEBUG"
      - "--providers.docker.exposedByDefault=false"
      - "--entryPoints.web.address=:80"
      - "--entryPoints.websecure.address=:443"
      - "--certificatesResolvers.le.acme.email=${ACME_EMAIL}"
      - "--certificatesResolvers.le.acme.storage=/acme/acme.json"
      - "--certificatesResolvers.le.acme.httpChallenge=true"
      - "--certificatesResolvers.le.acme.httpChallenge.entryPoint=web"
      - "--certificatesresolvers.le.acme.caserver=https://acme-v02.api.letsencrypt.org/directory"
    ports:
      - "443:443"
      - "80:80"
      # The Web UI (enabled by --api.insecure=true)
      - "8082:8080"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - acme:/acme
    networks:
      adventure_net:

  front:
    image: thecodingmachine/workadventure-front:v1.12.10
    environment:
      DEBUG_MODE: "false"
      JITSI_URL: "meet.jit.si"
      JITSI_PRIVATE_MODE: "false"
      PUSHER_URL: https://${PUSHER_URL}
      CHAT_URL: https://${CHAT_URL}
      ICON_URL: https://${ICON_URL}
      # CHANGE ME -----------------------------------------------------
      TURN_SERVER: ${TURN_SERVER}
      TURN_USER: ${TURN_USER}
      TURN_PASSWORD: ${TURN_PASSWORD}
      MAX_PER_GROUP: 4
      MAX_USERNAME_LENGTH: 15
      START_ROOM_URL: ${START_ROOM_URL}
      ENABLE_OPENID: ${ENABLE_OPENID}
      DISABLE_ANONYMOUS: ${DISABLE_ANONYMOUS}
      ENABLE_CHAT: ${ENABLE_CHAT}
    labels:
      - traefik.enable=true
      - traefik.http.routers.adventure-front.rule=Host(`play.${DOMAIN}`)
      - traefik.http.routers.adventure-front.entryPoints=web
      - traefik.http.services.adventure-front.loadbalancer.server.port=80
      - traefik.http.routers.adventure-front.middlewares=https_redirect

      - traefik.http.routers.adventure-front-ssl.rule=Host(`play.${DOMAIN}`)
      - traefik.http.routers.adventure-front-ssl.entryPoints=websecure
      - traefik.http.routers.adventure-front-ssl.tls=true
      - traefik.http.routers.adventure-front-ssl.service=adventure-front
      - traefik.http.routers.adventure-front-ssl.tls.certresolver=le
      - traefik.http.routers.adventure-front-ssl.middlewares=redir
      # middlewares
      - traefik.http.middlewares.redir.redirectregex.regex=^https://www\.(.+)
      - traefik.http.middlewares.redir.redirectregex.replacement=https://$${1}
      - traefik.http.middlewares.redir.redirectregex.permanent=true
      - traefik.http.middlewares.https_redirect.redirectscheme.scheme=https
      - traefik.http.middlewares.https_redirect.redirectscheme.permanent=true
    restart: unless-stopped
    networks:
      adventure_net:

  pusher:
    image: thecodingmachine/workadventure-pusher:v1.12.10
    environment:
      # SECRET_JITSI_KEY: "${SECRET_JITSI_KEY}"
      SECRET_KEY: yourSecretKey
      API_URL: back:50051
      # ADMIN_API_URL: "${ADMIN_API_URL}"
      # ADMIN_API_TOKEN: "${ADMIN_API_TOKEN}"
      JITSI_URL: ${JITSI_URL}
      # JITSI_ISS: ${JITSI_ISS}
      FRONT_URL : ${FRONT_URL}
      ENABLE_CHAT: ${ENABLE_CHAT}
      OPID_CLIENT_ID: ${OPID_CLIENT_ID}
      OPID_CLIENT_SECRET: ${OPID_CLIENT_SECRET}
      OPID_CLIENT_ISSUER: ${OPID_CLIENT_ISSUER}
      OPID_CLIENT_REDIRECT_URL: ${OPID_CLIENT_REDIRECT_URL}
      OPID_PROFILE_SCREEN_PROVIDER: ${OPID_PROFILE_SCREEN_PROVIDER}
      DISABLE_ANONYMOUS: ${DISABLE_ANONYMOUS}
      # OPID_USERNAME_CLAIM: ${OPID_USERNAME_CLAIM}
      EJABBERD_API_URI: http://ejabberd:5443/api
      EJABBERD_DOMAIN: ejabberd
      EJABBERD_WS_URI: ws://ejabberd:5443/ws
      EJABBERD_JWT_SECRET: mySecretJwtToken
      START_ROOM_URL: "/_/global/garagenum.github.io/garage-wa-map/map.json"
    labels:
      - traefik.enable=true
      - traefik.http.routers.adventure-pusher.rule=Host(`pusher.${DOMAIN}`)
      - traefik.http.routers.adventure-pusher.entryPoints=web
      - traefik.http.services.adventure-pusher.loadbalancer.server.port=8080
      - traefik.http.routers.adventure-pusher-ssl.rule=Host(`pusher.${DOMAIN}`)
      - traefik.http.routers.adventure-pusher-ssl.entryPoints=websecure
      - traefik.http.routers.adventure-pusher-ssl.tls=true
      - traefik.http.routers.adventure-pusher-ssl.service=adventure-pusher
      - traefik.http.routers.adventure-pusher-ssl.tls.certresolver=le
    restart: unless-stopped
    networks:
      adventure_net:

  back:
    image: thecodingmachine/workadventure-back:v1.12.10
    command: yarn run runprod
    environment:
      # SECRET_KEY: ${SECRET_KEY}
      # SECRET_JITSI_KEY: "${SECRET_JITSI_KEY}"
      ADMIN_API_TOKEN: "${ADMIN_API_TOKEN}"
      ADMIN_API_URL: "${ADMIN_API_URL}"
      JITSI_URL: ${JITSI_URL}
      JITSI_ISS: ""
      MAX_PER_GROUP: 4
      # TURN_STATIC_AUTH_SECRET: "${TURN_STATIC_AUTH_SECRET}"
      REDIS_HOST: redis
      ENABLE_CHAT: ${ENABLE_CHAT}
      EJABBERD_API_URI: http://ejabberd:5443/api
      EJABBERD_DOMAIN: //xmpp.${DOMAIN}
    labels:
      - traefik.enable=true
      - traefik.http.routers.adventure-back.rule=Host(`api.${DOMAIN}`)
      - traefik.http.routers.adventure-back.entryPoints=web
      - traefik.http.services.adventure-back.loadbalancer.server.port=8080
      - traefik.http.routers.adventure-back-ssl.rule=Host(`api.${DOMAIN}`)
      - traefik.http.routers.adventure-back-ssl.entryPoints=websecure
      - traefik.http.routers.adventure-back-ssl.tls=true
      - traefik.http.routers.adventure-back-ssl.service=adventure-back
      - traefik.http.routers.adventure-back-ssl.tls.certresolver=le
    restart: unless-stopped
    networks:
      adventure_net:

# wont work for now
  uploader:
    image: thecodingmachine/workadventure-uploader:v1.12.10
    environment:
      UPLOADER_URL: //uploader
      #REDIS
      REDIS_HOST: redis
      REDIS_PORT: 6379
      UPLOADER_REDIS_DB_NUMBER: 0
      #CHAT
      ADMIN_API_URL: 
      ENABLE_CHAT_UPLOAD: "false"
      UPLOAD_MAX_FILESIZE: 10485760
    labels:
      - traefik.enable=true
      - traefik.http.routers.adventure-uploader.rule=Host(`uploader.${DOMAIN}`)
      - traefik.http.routers.adventure-uploader.entryPoints=web
      - traefik.http.services.adventure-uploader.loadbalancer.server.port=8080
      - traefik.http.routers.adventure-uploader-ssl.rule=Host(`uploader.${DOMAIN}`)
      - traefik.http.routers.adventure-uploader-ssl.entryPoints=websecure
      - traefik.http.routers.adventure-uploader-ssl.service=adventure-uploader
      - traefik.http.routers.adventure-uploader-ssl.tls=true
      - traefik.http.routers.adventure-uploader-ssl.tls.certresolver=le
    networks:
      adventure_net:

  chat:
    image: thecodingmachine/workadventure-chat:v1.12.10
    environment:
      PUSHER_URL: //pusher.${DOMAIN}
      UPLOADER_URL: //uploader.${DOMAIN}
      # EMBEDLY_KEY: ${EMBEDLY_KEY}
      ENABLE_CHAT_UPLOAD: "false"
      EJABBERD_DOMAIN: //xmpp.${DOMAIN}
      ENABLE_OPENID: 1

    labels:
      - traefik.enable=true
      - traefik.http.routers.adventure-chat.rule=Host(`chat.${DOMAIN}`)
      - traefik.http.routers.adventure-chat.entryPoints=web
      - traefik.http.services.adventure-chat.loadbalancer.server.port=80
      - traefik.http.routers.adventure-chat-ssl.rule=Host(`chat.${DOMAIN}`)
      - traefik.http.routers.adventure-chat-ssl.entryPoints=websecure
      - traefik.http.routers.adventure-chat-ssl.service=adventure-chat
      - traefik.http.routers.adventure-chat-ssl.tls=true
      - traefik.http.routers.adventure-chat-ssl.tls.certresolver=le
    networks:
      adventure_net:

  icon:
    image: matthiasluedtke/iconserver:v3.13.0
    labels:
      - traefik.enable=true
      - traefik.http.routers.adventure-icon.rule=Host(`icon.${DOMAIN}`)
      - traefik.http.routers.adventure-icon.entryPoints=web,traefik
      - traefik.http.services.adventure-icon.loadbalancer.server.port=8080
      - traefik.http.routers.adventure-icon-ssl.rule=Host(`icon.${DOMAIN}`)
      - traefik.http.routers.adventure-icon-ssl.entryPoints=websecure
      - traefik.http.routers.adventure-icon-ssl.tls=true
      - traefik.http.routers.adventure-icon-ssl.service=adventure-icon
      - traefik.http.routers.adventure-icon-ssl.tls.certresolver=le
    networks:
      adventure_net:

  ejabberd:
    container_name: adventure-ejabberd
    image: ghcr.io/processone/ejabberd:22.05
    networks:
      adventure_net:
    volumes:
      - ./ejabberd.yml:/opt/ejabberd/conf/ejabberd.yml
    labels:
      - traefik.enable=true
      - traefik.http.routers.xmpp.rule=Host(`xmpp.${DOMAIN}`)
      - traefik.http.routers.xmpp.entryPoints=web
      - traefik.http.services.xmpp.loadbalancer.server.port=5380
      - traefik.http.routers.xmpp-ssl.rule=Host(`xmpp.${DOMAIN}`)
      - traefik.http.routers.xmpp-ssl.entryPoints=websecure
      - traefik.http.routers.xmpp-ssl.tls=true
      - traefik.http.routers.xmpp-ssl.service=xmpp


  redis:
    container_name: adventure-redis
    image: redis:6
    volumes:
      - redisdata:/data
    networks:
      adventure_net:


  coturn:
    image: coturn/coturn:4.5.2
    command:
      - turnserver
      #- -c=/etc/coturn/turnserver.conf
      - --log-file=stdout

      # change me
      - --external-ip=92.243.20.228
      - --realm=turn.${DOMAIN}
      - --server-name=turn.${DOMAIN}

      - --listening-port=3478
      - --min-port=10002
      - --max-port=10100
      - --tls-listening-port=5349
      - --listening-ip=0.0.0.0
      - --lt-cred-mech
      # Enable Coturn REST API to validate temporary passwords.
      #- --use-auth-secret
      #- --static-auth-secret=SomeStaticAuthSecret
      #- --userdb=/var/lib/turn/turndb
      - --user=workadventure:WorkAdventure123
      # use real-valid certificate/privatekey files
      #- --cert=/root/letsencrypt/fullchain.pem
      #- --pkey=/root/letsencrypt/privkey.pem
    network_mode: host


networks:
  adventure_net:

volumes:
  acme:
  redisdata: